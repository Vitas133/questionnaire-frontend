import { configureStore, getDefaultMiddleware, Store } from '@reduxjs/toolkit';
import rootReducer from './reducers';

function initStore() {
  const store: Store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware({
      serializableCheck: false,
    }),
  });

  return store;
}

const store = initStore();

export default store;
