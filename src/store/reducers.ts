import { combineReducers } from '@reduxjs/toolkit';
import { questionnaire } from './questionnaire/questionnaire.reducer';

export default combineReducers({
  questionnaire,
});
