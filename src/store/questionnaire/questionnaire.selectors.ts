import { RootState } from '../RootState';
import { QuestionnaireState } from './questionnaire.types';

export const questionnaireSummarySelectors = (state: RootState): QuestionnaireState =>
  state.questionnaire;