import { ProjectType } from '../../types/ProjectType';
import { DateTime } from '../../types/DateTime';

export interface QuestionnaireState {
    title: string;
    companyLogo: string;
    type: ProjectType | '';
    budget: number;
    requiredPeople: number;
    timeline: DateTime;
    isQaRequired: boolean;
}
