import { createReducer } from '@reduxjs/toolkit';
import { QuestionnaireState } from './questionnaire.types';
import { setNewQuestionnaire } from './questionnaire.actions';

const initialState: QuestionnaireState = {
  title: '',
  type: '',
  companyLogo: '',
  budget: 0,
  requiredPeople: 0,
  timeline: '',
  isQaRequired: false
};

export const questionnaire = createReducer(initialState, builder =>
  builder
    .addCase(setNewQuestionnaire, (state, { payload }) => {
      state.title = payload.title;
      state.type = payload.type;
      state.companyLogo = payload.companyLogo;
      state.budget = payload.budget;
      state.requiredPeople = payload.requiredPeople;
      state.timeline = payload.timeline;
      state.isQaRequired = payload.isQaRequired;
    }),
);
