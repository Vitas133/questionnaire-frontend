import { createAction } from '@reduxjs/toolkit';
import { QuestionnaireState } from './questionnaire.types';

export const setNewQuestionnaire = createAction<QuestionnaireState>('questionnaire/setNewQuestionnaire');