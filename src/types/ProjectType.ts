export enum ProjectType {
  IT = 'IT',
  BUSINESS = 'BUSINESS',
  FOOD_QUALITY = 'FOOD_QUALITY',
}
