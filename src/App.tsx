import './App.css';
import { StyleSheetManager, ThemeProvider } from 'styled-components';
import store from './store/store';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { AppRouter } from './AppRouter';
import { theme } from './utils/theme';

export const App = () => (
    <StyleSheetManager>
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <Router>
            <AppRouter />
          </Router>
        </Provider>
      </ThemeProvider>
    </StyleSheetManager>
  )
