import { createMuiTheme } from '@material-ui/core';

const palette = {
  background: {
    default: '#323b44',
  },
  primary: {
    main: '#83C438',
  },
  secondary: {
    main: '#F29307',
  },
  text: {
    primary: '#4a4a4a',
  },
};

export const theme = createMuiTheme({
  palette,
  props: {
    MuiButtonBase: {
      disableRipple: true,
    },
    MuiButton: {
      disableElevation: true,
    },
  },
  overrides: {
    MuiChip: {
      outlined: {
        border: 'solid 1px #ddd',
        backgroundColor: '#fff',
        color: '#555',
        '&$clickable': {
          '&:focus': {
            backgroundColor: '#fff',
          },
        },
        '& .MuiTouchRipple-root span': {
          backgroundColor: '#83c438',
        },
      },
      outlinedPrimary: {
        color: '#fff',
        border: 'solid 1px #74AD31',
        backgroundColor: '#83C438',
        '&$clickable': {
          '&:focus, &:hover': {
            backgroundColor: '#83C438',
          },
        },
      },
      deleteIconOutlinedColorPrimary: {
        color: '#fff',
        '&:focus, &:hover': {
          color: '#e3e9e3',
        },
      },
    },
    MuiButton: {
      root: {
        color: '#555',
        borderRadius: '4px',
        padding: '8px 16px',
        backgroundColor: 'transparent',
        whiteSpace: 'nowrap',
        height: '32px',
        minWidth: 'auto',
        lineHeight: '14px',
        '&$disabled': {
          backgroundColor: 'transparent',
          borderColor: '#ddd',
          color: '#A0A0A0',
        },
      },
      outlined: {
        padding: '8px 16px',
      },
      text: {
        padding: '8px 16px',
      },
      containedPrimary: {
        color: '#fff',
        border: 'solid 1px #74AD31',
        backgroundColor: '#83C438',
        '&:hover': {
          backgroundColor: '#74AD31',
        },
        '&$disabled': {
          background: 'transparent',
          borderColor: '#ddd',
        },
      },
      startIcon: {
        marginRight: '4px',
      },
    },
    MuiTable: {
      root: {
        borderLeft: 'solid 1px #e0e0e0',
        borderRight: 'solid 1px #e0e0e0',
        borderTop: 'solid 1px #e0e0e0',
      },
    },
    MuiTableRow: {
      root: {
        background: '#f8f8f8',
        '&:nth-of-type(even)': {
          backgroundColor: '#f3f3f3',
        },
        '&$hover:hover': {
          backgroundColor: '#eaeaea !important',
        },
        '&$selected': {
          backgroundColor: '#fcfcd1',
        },
      },
      head: {
        background: '#eff0f6',
      },
    },
    MuiTableCell: {
      root: {
        fontSize: '0.875rem',
      },
      head: {
        color: '#767678',
        fontWeight: 'normal',
      },
      body: {
        color: '#555',
      },
    },
  },
});
