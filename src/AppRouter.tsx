import { Redirect, Route, Switch } from 'react-router-dom';
import { QuestionnaireView } from './views/QuestionnaireView';

export const AppRouter = () => (
  <Switch>
    <Redirect exact from='/' to='/questionnaire' />
    <Route path='/questionnaire' component={QuestionnaireView} />
  </Switch>
);
