import { Grid, withStyles } from '@material-ui/core';
import styled from 'styled-components';

export const GridContainer = withStyles(theme => ({
  root: {
    textAlign: 'center',
    margin: theme.spacing(2)
  },
}))(Grid);

export const CustomForm = styled.form`
  max-width: 800px;
  margin: auto;
  font-size: 0.8rem;
  color: #6f6f6f;
  font-weight: 300;
`;