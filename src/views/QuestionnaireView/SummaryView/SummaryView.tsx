import { Box } from '@material-ui/core';
import { QuestionnaireState } from '../../../store/questionnaire/questionnaire.types';
import { ImgPreview } from './SummaryView.styles';

interface SummaryViewProps {
  questionnaireData: QuestionnaireState;
}

export const SummaryView = ({ questionnaireData }: SummaryViewProps) => (
  <Box fontSize={20}>
    Check your answers:
    <Box p={1} maxWidth={400} maxHeight={400}>
      Logo: <ImgPreview title='Company logo' src={questionnaireData.companyLogo} alt='logo' />
    </Box>
    <Box p={1}>
      Title: {questionnaireData.title}
    </Box>
    <Box p={1}>
      Type of project: {questionnaireData.title}
    </Box>
    <Box p={1}>
      Your timeline: {questionnaireData.timeline}
    </Box>
    <Box p={1}>
      Is QA required in you project: {questionnaireData.isQaRequired ? 'YES' : 'NO'}
    </Box>
    <Box p={1}>
      Number of required people: {questionnaireData.requiredPeople}
    </Box>
    <Box p={1}>
      Your budget: {questionnaireData.budget}
    </Box>
  </Box>
);