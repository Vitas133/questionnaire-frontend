import styled from 'styled-components';

export const ImgPreview = styled.img`
  max-width: inherit;
  max-height: inherit;
`;