import { useFormik } from 'formik';
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { validationSchema } from './QuestionnaireView.validation';
import { setNewQuestionnaire } from '../../store/questionnaire/questionnaire.actions';
import { AppDispatch } from '../../store/AppDispatch';
import { QuestionnaireState } from '../../store/questionnaire/questionnaire.types';

const initialValues: QuestionnaireState = {
  title: '',
  type: '',
  companyLogo: '',
  budget: 0,
  requiredPeople: 0,
  timeline: '',
  isQaRequired: false,
};

export const useQuestionnaireForm = () => {
  const dispatch = useDispatch<AppDispatch>();

  const onSubmit = useCallback(
    (data: QuestionnaireState) => {
      dispatch(setNewQuestionnaire(data));
    },
    [dispatch],
  );

  const formik = useFormik({
    enableReinitialize: true,
    initialValues,
    validationSchema,
    onSubmit,
  });

  const handleSelect = useCallback(({ target: {value} }) => {
    formik.setFieldValue('type', value);
  }, [formik]);

  const handleUploadFile = useCallback((event) => {
    const [file] = event.target.files;
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onloadend = () => {
      if (reader.result) {
        formik.setFieldValue('companyLogo', reader.result);
      }
    };
  }, [formik]);

  return { ...formik, handleUploadFile, handleSelect };
};
