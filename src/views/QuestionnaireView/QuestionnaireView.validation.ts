import * as yup from 'yup';

export const validationSchema = yup.object().shape({
  title: yup.string().required(),
  type: yup.string().required(),
  companyLogo: yup.string().required(),
  budget: yup.number().required().min(1),
  requiredPeople: yup.number().required().min(1),
  timeline: yup.string().required(),
  isQaRequired: yup.boolean().required(),
});
