import {render, fireEvent, wait} from '@testing-library/react'
import React from 'react'
import { App } from '../../App';

describe('views:Questionnaire', async () => {
  it("should find all inputs", async () => {
    const result = render(<App />);

    const title = result.container.querySelector('#title');
    const companyLogo = result.container.querySelector('#companyLogo');
    const type = result.container.querySelector('#type');
    const budget = result.container.querySelector('#budget');
    const requiredPeople = result.container.querySelector('#requiredPeople');
    const timeline = result.container.querySelector('#timeline');
    const isQaRequired = result.container.querySelector('#isQaRequired');


    expect(title).toBeVisible();
    expect(companyLogo).toBeVisible();
    expect(type).toBeVisible();
    expect(budget).toBeVisible();
    expect(requiredPeople).toBeVisible();
    expect(timeline).toBeVisible();
    expect(isQaRequired).toBeVisible();
  });
});
