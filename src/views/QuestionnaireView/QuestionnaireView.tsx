import { Box, Button, FormControlLabel, Grid, MenuItem, Switch, TextField } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { useQuestionnaireForm } from './useQuestionnaireForm';
import { CustomForm, GridContainer } from './QuestionnaireView.styles';
import { questionnaireSummarySelectors } from '../../store/questionnaire/questionnaire.selectors';
import { SummaryView } from './SummaryView/SummaryView';
import { ProjectType } from '../../types/ProjectType';

export const QuestionnaireView = () => {
  const questionnaireSummary = useSelector(questionnaireSummarySelectors);

  const { handleSubmit, handleChange, values, errors, touched, handleUploadFile } = useQuestionnaireForm();

  return (
    <CustomForm onSubmit={handleSubmit}>
      <GridContainer container spacing={2}>
        <Grid item xs={12}>
          <Box component='h1'>
            Please enter requirements for you new project
          </Box>
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            variant='outlined'
            label='Enter project title'
            value={values.title}
            name='title'
            id='title'
            onChange={handleChange}
            error={Boolean(touched.title && errors.title)}
            helperText={touched.title && errors.title}
            autoFocus
            fullWidth
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            type='file'
            inputProps={{ accept: 'image/*' }}
            variant='outlined'
            id='companyLogo'
            onChange={handleUploadFile}
            error={Boolean(touched.companyLogo && errors.companyLogo)}
            helperText={touched.companyLogo && errors.companyLogo}
            autoFocus
            fullWidth
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            variant='outlined'
            name='type'
            id='type'
            select
            label='Select project type'
            value={values.type}
            error={Boolean(touched.type && errors.type)}
            helperText={touched.type && errors.type}
            autoFocus
            fullWidth
            onChange={handleChange}

          >
            {Object.values(ProjectType).map(value => (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            type='number'
            variant='outlined'
            label='Enter project budget'
            value={values.budget}
            name='budget'
            id='budget'
            onChange={handleChange}
            error={Boolean(touched.budget && errors.budget)}
            helperText={touched.budget && errors.budget}
            autoFocus
            fullWidth
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            type='number'
            variant='outlined'
            label='How many people is required for this project?'
            value={values.requiredPeople}
            name='requiredPeople'
            id='requiredPeople'
            onChange={handleChange}
            error={Boolean(touched.requiredPeople && errors.requiredPeople)}
            helperText={touched.requiredPeople && errors.requiredPeople}
            autoFocus
            fullWidth
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            variant='outlined'
            label='Enter timeline of you project'
            value={values.timeline}
            name='timeline'
            id='timeline'
            onChange={handleChange}
            error={Boolean(touched.timeline && errors.timeline)}
            helperText={touched.timeline && errors.timeline}
            autoFocus
            fullWidth
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <FormControlLabel
            id="isQaRequired"
            control={
              <Switch
                checked={values.isQaRequired}
                onChange={handleChange}
                name="isQaRequired"
                color="secondary"
              />
            }
            label="Is QA required in your project?"

          />
        </Grid>
        <Grid item xs={12}>
          <Box display='flex' justifyContent='flex-end'>
            <Box ml={1}>
              <Button color='primary' variant='outlined' type='submit'>
                Submit
              </Button>
            </Box>
          </Box>
        </Grid>
      </GridContainer>

      {questionnaireSummary.title && <SummaryView questionnaireData={questionnaireSummary} />}
    </CustomForm>
  );
};
